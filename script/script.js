"use strict";

let inputNumber = +prompt("Please, enter your number:");

function validation () {
    while (isNaN(inputNumber) || inputNumber === "" || inputNumber <= 0) {
        alert("You have not entered a number or entered incorrect one!!")

        inputNumber = +prompt("Please, enter correct first number:");
    }
}
validation();

function factorialSolution (number) {
    if (number === 1) {
        return 1;
    } else {
        return number * factorialSolution(number - 1);
    }
}

console.log(factorialSolution(inputNumber));
